# REST API

**What is REST API?**

REST stands for Representational State Transfer. It is an architectural style for providing standards between computer systems on the web, making it efficient and comfortable to communicate with each other. These systems are called RESTful systems and they are characterized by their statelessness and the separation of concerns between client and server. A RESTful system consists of:

- Client: who makes requests for the data.
- Server: who gets the data and returns it to the client.

**Separation**

In these systems, both the implementation of clients and servers is done separately without each knowing about the other. So that the client code can be easily changed without affecting the server and vice versa. But this can only be done when both sides know the format of messages. It also separates data concerns from clients, which helps improve the flexibility of the system across platforms and improves scalability by simplifying server components. It also allows each component to develop separately without interfering with each other. Even with the separation, the clients hit the same endpoint to perform the same actions to receive the same response.

**Statelessness**

REST is stateless in architecture, meaning that the server and client do not necessarily require knowledge about the state of each other. They can easily work without knowing previous messages and can understand any message received. This helps REST applications to give quick performance. With the help of this, we can increase reliability and increase scalability of the system as components that can be managed, updated, and reused without affecting the system as a whole.

**There are six architectural constraints of RESTful APIs:**

1. **Uniform Interface**: This principle means that all interactions between clients (like your computer or phone) and servers (where websites or services are hosted) follow a standard set of rules. This makes it easier for different parts of the internet to talk to each other.

2. **Statelessness**: Each time your device talks to a server, it's like a new conversation. The server doesn't remember past interactions with your device, making things simpler and more reliable.

3. **Cacheable**: Servers can tell your device to remember certain information for a while. So, if you ask for it again, your device can get it from its memory instead of asking the server again, making things faster.

4. **Client-Server**: In REST, your device (the client) asks for things from the server, and the server sends back what you need. You don't need to know how the server works; you just need to know how to ask for things.

5. **Layered System**: In REST, your device might send a request through several intermediary servers before reaching the final destination. Each server helps move the request along, but you don't need to worry about how they do it.

6. **Code on Demand (Optional)**: In REST, servers can send your device code (like JavaScript) to run and enhance your experience. This is optional and not always used, but it can make interactions more dynamic and exciting.

**Request**

REST requires that a client make a request to the server in order to fetch the data from the server. A request contains:

- HTTP verb which defines the kind of operation.
- Header which allows the client to pass information.
- Path
- Messages

HTTP gives us four methods for making requests:

1. **GET**: Retrieves data from the server using an id or other parameter.
2. **POST**: Helps to create a new data set on the server or the database.
3. **PUT**: Helps in updating the previously saved dataset.
4. **DELETE**: Helps in removing data from the database.

Combining all these requests, we can easily perform CRUD operations (Create, Read, Update, Delete).

**Conclusion**

In conclusion, REST is a powerful architectural tool for designing vast systems that can be easily evolved with time without hindering the client experience and is easy to maintain the whole system with efficient working. REST can help in increasing the scalability and reliability of the whole vast system. It offers a vast robust framework for designing web applications to meet the modern web requirements.

**References:**

- [Codecademy - What is REST](https://www.codecademy.com/article/what-is-rest)
- [GeeksforGeeks - REST API Architectural Constraints](https://www.geeksforgeeks.org/rest-api-architectural-constraints/)
- [YouTube - REST API Concepts and Examples](https://www.youtube.com/watch?v=ukIE6Af4v5k)
