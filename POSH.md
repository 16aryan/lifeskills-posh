# Prevention of Sexual Harassment Policy

## Introduction

At MountBlue, we are committed to providing a safe and respectful work environment for all employees. Sexual harassment in the workplace is unacceptable and will not be tolerated under any circumstances. This policy outlines our commitment to preventing sexual harassment and the procedures for reporting and addressing any incidents that occur.

## Policy Statement

We are committed to maintaining a workplace free from sexual harassment. Sexual harassment, whether verbal, physical, or visual, is prohibited and will result in disciplinary action, up to and including termination of employment.

## Definition of Sexual Harassment

Sexual harassment is defined as any unwelcome conduct of a sexual nature, including but not limited to:

- Verbal harassment, such as sexual advances, comments, jokes, or propositions.
- Non-verbal harassment, such as suggestive gestures or inappropriate physical contact.
- Visual harassment, such as displaying sexually explicit images or materials.

## Reporting Procedures

Employees who experience or witness sexual harassment are encouraged to report the incident immediately. Reporting can be done through the following channels:

1. Directly to a supervisor or manager.
2. Human Resources department.
3. Anonymous reporting hotline or online platform.

All reports of sexual harassment will be taken seriously and handled with discretion. Retaliation against employees who report sexual harassment is strictly prohibited.

## Investigation and Resolution

Upon receiving a report of sexual harassment, MountBlue will promptly investigate the matter in a fair and impartial manner. Investigations may include interviews with the parties involved, as well as any witnesses. The findings of the investigation will be communicated to the relevant parties, and appropriate action will be taken based on the severity of the harassment.

## Training and Awareness

MountBlue is committed to providing training and awareness programs to educate employees about sexual harassment prevention. Training sessions will cover topics such as recognizing harassment, bystander intervention, and reporting procedures.

## Conclusion

Preventing sexual harassment in the workplace is the responsibility of every employee. By adhering to this policy and fostering a culture of respect and professionalism, we can create a safe and inclusive work environment for all.



