### Question 1: Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.

**Answer**  
Angela Duckworth highlights the need for "grit," a combination of enthusiasm and tenacity for long-term goals, as a key component in predicting success. She emphasizes the importance of understanding and nurturing grit, particularly in education, and proposes exploring the concept of a "growth mindset." Duckworth advocates for ongoing efforts to test and refine methods for developing grit in children.

### Question 2: Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.

**Answer**  
The video explores different approaches to learning, focusing on the concept of a "growth mindset." It contrasts two types of thinking: fixed mindset and growth mindset. Fixed mindsets believe that skills are innate and unchangeable, while growth mindsets believe that abilities can be developed. According to the video, having a growth mindset enhances learning by viewing effort, challenges, failures, and feedback as opportunities for growth. It underscores the importance of cultivating a growth mindset for improved learning in various contexts such as academics, sports, and the workplace.

### Question 3: What is the Internal Locus of Control? What is the key point in the video?

**Answer**  
The internal locus of control refers to the belief that one has control over their life outcomes. The video discusses a study where children who were praised for their hard work became more motivated and enjoyed challenges. This aligns with having an internal locus of control, where individuals feel empowered to influence their circumstances. The key point is that believing in one's ability to control outcomes leads to increased motivation and achievement. To foster this mindset, tackle challenges proactively and recognize the impact of your actions.

### Question 4: What are the key points mentioned by speaker to build growth mindset (explanation not needed).

**Answer**  
The speaker outlines several key points to cultivate a growth mindset:

1. **Believe in Your Ability to Learn**: Have confidence in your capacity to figure things out.
2. **Challenge Assumptions**: Question limiting beliefs and assumptions.
3. **Create a Learning Plan**: Develop your own curriculum to foster continuous learning.
4. **Embrace Struggle**: Value the struggle as an integral part of the learning process.
5. **Cultivate Resilience**: Maintain resilience in the face of challenges and setbacks.

### Question 5:What are your ideas to take action and build Growth Mindset?
**Answer**  
Here are some ideas to take action and build a growth mindset:

- **Take Ownership of Learning**: Assume full responsibility for your learning journey.
- **Persist Through Challenges**: Refuse to give up when faced with difficulties; persevere until you find a solution.
- **Engage Deeply**: Put in the effort to thoroughly understand concepts and materials.
- **Seek Opportunities in Challenges**: View challenges as opportunities for growth and improvement.
- **Reflect and Learn from Setbacks**: Embrace setbacks as learning experiences and use them to fuel personal growth.
