### Question 1
#### Active Listening Techniques 
- Pay attention to the speaker's words and the subject matter.
- Refrain from interrupting.
- Utilize open-ended questions.
- Make written notes.
- Recapitulate to confirm understanding.
- Demonstrate interest and empathy.

### Question 2
#### Key Components of Reflective Listening (Fisher's model)
**Validation of Understanding:** Reflective listening involves affirming the speaker's perspective for accurate comprehension.
- **Founded in Empathy:** Originating from Carl Rogers' therapeutic approach, it emphasizes genuine empathy and understanding.
- **Applicability in Daily Life:** Beneficial in personal relationships, particularly for encouraging emotional expression.

### Question 3
#### Challenges in Listening Process
**Response:**
- Lack of empathy
- Presence of numerous distractions
- Preconceived notions
- Selective attention

<hr/>

### Question 4
#### Strategies to Enhance Listening Skills
**Response:**
1. Show genuine interest.
2. Practice kindness and understanding.
3. Refrain from hasty judgments.
4. Minimize external distractions.

### Question 5
#### Instances of Adopting a Passive Communication Style
**Response:** I resort to passive communication when aiming to maintain a harmonious atmosphere and avoid confrontation. While I value open dialogue, preserving positive connections sometimes takes precedence over assertively expressing my views.

### Question 6
##### Situations Prompting Aggressive Communication Styles
**Response:** In my day-to-day life, I may adopt an aggressive communication style when feeling overwhelmed or experiencing negative emotions. During such instances, I might articulate my thoughts forcefully or exhibit heightened reactions.

### Question 7
#### Engagement in Passive Aggressive Communication Styles
**Response:** I resort to passive-aggressive communication tactics like sarcasm or the silent treatment when feeling frustrated or resentful. This typically occurs when I find it challenging to express my sentiments directly.

### Question 8
#### Cultivating Assertive Communication Skills
**Response:**
- **Expressing My Emotions:** I'll articulate my feelings without assigning blame, fostering open dialogue.
- **Identifying My Needs:** I'll ascertain my true requirements in various situations and communicate them clearly.
- **Practicing in Low-Stakes Scenarios:** I'll build confidence by asserting myself in less challenging situations.
- **Observing Nonverbal Cues:** I'll pay attention to my body language and tone, ensuring alignment with my verbal communication.
- **Addressing Issues Proactively:** I'll address concerns promptly to expedite resolutions.
